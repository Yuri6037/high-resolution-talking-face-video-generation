import argparse
import os
import subprocess
import shutil

# Reusable pipeline phases
def video_decomposition_phase(video_name):
    print("Running video decomposition phase...")
    outpath = os.path.join("Data", video_name + "_src")
    if os.path.isdir(outpath):
        shutil.rmtree(outpath)
    path = os.path.join("Data", video_name + ".mov")
    env = os.environ.copy()
    env["LD_LIBRARY_PATH"] = "./FrameUtils/CInterface:./lib"  # Setup library path to both libCInterface.so and FFMPEG
    subprocess.run(["./FrameUtils/frame-utils/target/release/frame-utils", "decompose", "-i", path, "-o", outpath], env=env)
    print("Decompiled video: '" + video_name + "' in: " + outpath)
    return outpath

def video_recomposition_phase(video_name, pipeline_name):
    print("Running video recomposition phase...")
    videosrc = os.path.join("Data", video_name + "_src")
    path = os.path.join("Data", video_name + ".mov")
    outpath = os.path.join("result", video_name + "_" + pipeline_name + ".mov")
    env = os.environ.copy()
    env["LD_LIBRARY_PATH"] = "./FrameUtils/CInterface:./lib"  # Setup library path to both libCInterface.so and FFMPEG
    subprocess.run(["./FrameUtils/frame-utils/target/release/frame-utils", "recompose", "-i", videosrc, "-s", path, "-o", outpath,
                    "--scale", "2"], env=env)
    print("Recompiled video: " + outpath)

def gfpgan_restoration_phase(videosrc, iterations=1):
    if iterations <= 0:
        return
    print("Running gfpgan restoration phase...")
    subprocess.run(["python3", "GFPGAN/inference_gfpgan.py", "--upscale", "1", "--iterations", str(iterations),
                    "--in-place", "--test_path", videosrc,
                    "--model_path", "GFPGAN/experiments/pretrained_models/GFPGANCleanv1-NoCE-C2.pth"])

def san_upscale_phase(videosrc, batch, model_path):
    print("Running SAN upscale phase...")
    result_dir = os.path.join("result", "san")
    for file in os.listdir(videosrc):
        if file.endswith(".png"):
            file = os.path.join(videosrc, file)
            subprocess.run(["python3", "SAN/NewCode/main.py", "--upscale", "--scale", "2", "--use-bgr",
                            "--batch", str(batch), "--model-path", model_path, "--image", file])
    for file in os.listdir(videosrc):
        if file.endswith(".png"):
            srcfile = os.path.join(videosrc, file)
            file = os.path.join(result_dir, file)
            os.remove(srcfile)  # Just in case shutil.copy is broken and doesn't auto-replace files
            shutil.copy(file, srcfile)

def prepare_finetune_set(videosrc, video_name):
    finetune_dir = os.path.join("Data", "finetune_" + video_name)
    train_dir = os.path.join(finetune_dir, "train")
    val_dir = os.path.join(finetune_dir, "val")
    if os.path.isdir(train_dir) and os.path.isdir(val_dir):
        return train_dir, val_dir
    os.mkdir(finetune_dir)
    os.mkdir(train_dir)
    os.mkdir(val_dir)
    count = 0
    for file in os.listdir(videosrc):
        if file.endswith(".png"):
            file = os.path.join(videosrc, file)
            if count % 3: # 1/3 of the finetune set is gonna be validation
                shutil.copy(file, train_dir)
            else:
                shutil.copy(file, val_dir)
            count += 1
    return train_dir, val_dir

# Actual pipeline variants
def solo_gfpgan(video_name, restore_iters=-1):
    if restore_iters == -1:
        restore_iters = 0
    videosrc = video_decomposition_phase(video_name)
    subprocess.run(["python3", "GFPGAN/inference_gfpgan.py", "--in-place", "--test_path", videosrc,
                    "--model_path", "GFPGAN/experiments/pretrained_models/GFPGANCleanv1-NoCE-C2.pth"])
    gfpgan_restoration_phase(videosrc, restore_iters)
    video_recomposition_phase(video_name, "solo_gfpgan_i" + str(restore_iters + 1))

def san_gfpgan(video_name, batch=8, restore_iters=-1):
    if restore_iters == -1:
        restore_iters = 1
    videosrc = video_decomposition_phase(video_name)
    san_upscale_phase(videosrc, batch, "SAN_hdtf")
    gfpgan_restoration_phase(videosrc, restore_iters)
    video_recomposition_phase(video_name, "san_gfpgan_i" + str(restore_iters))

def finetuned_san_gfpgan(video_name, train_batch=8, upscale_batch=8, restore_iters=-1):
    if restore_iters == -1:
        restore_iters = 1
    videosrc = video_decomposition_phase(video_name)
    train_dir, val_dir = prepare_finetune_set(videosrc, video_name)
    subprocess.run(["python3", "SAN/NewCode/main.py", "--train", "--model-path", "SAN_hdtf", "--scale", "2",
                    "--batch", str(train_batch), "--train-dir", train_dir, "--val-dir", val_dir, "--epochs", "10"])
    try: # Stupid python which prefers to crash the entire program rather than throwing a warning!
        os.remove("SAN_finetuned/SAN_BI2X.pt") # Just in case shutil.copy is broken and doesn't auto-replace files
    except:
        pass
    shutil.copy("experiment/save/model/model_e11.pt", "SAN_finetuned/SAN_BI2X.pt")
    san_upscale_phase(videosrc, upscale_batch, "SAN_finetuned")
    gfpgan_restoration_phase(videosrc, restore_iters)
    video_recomposition_phase(video_name, "finetuned_san_gfpgan_i" + str(restore_iters))

def main():
    parser = argparse.ArgumentParser(description='High Resolution Talking Face Video Generation')
    parser.add_argument('--restore-iters', type=int, help="Number of GFPGAN restoration iterations", default=-1)
    parser.add_argument('--upscale', help="Type of upscaling method", choices=['gfpgan', 'san', 'finetuned-san'])
    parser.add_argument('--input', type=str, help="Name of low resolution talking face video in Data directory")
    parser.add_argument('--batch-train', type=int, default=8, help="Batch size for SAN training")
    parser.add_argument('--batch', type=int, default=8, help="Batch size for SAN upscale")
    args = parser.parse_args()
    if args.upscale == "gfpgan":
        solo_gfpgan(args.input, args.restore_iters)
    elif args.upscale == "san":
        san_gfpgan(args.input, args.batch, args.restore_iters)
    elif args.upscale == "finetuned-san":
        finetuned_san_gfpgan(args.input, args.batch_train, args.batch, args.restore_iters)

main()
